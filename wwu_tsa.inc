<?php

/**
 * @file
 * Includes additional necessary functions for thundersearch to work.
 */

define('THUNDERSEARCH_MAIN_ENDPOINT', '/texis/search/main.html');

define('THUNDERSEARCH_DEFAULT_DRUPAL_PR', 'Default-WWU-Drupal');

define('THUNDERSEARCH_RESULT_URL', '/thundersearch/result');

/**
 * Thunderstone search form.
 */
function thundersearch_form($form, &$form_state, $query = '') {
  $form['#attributes'] = array(
    'class' => 'search-form',
  );

  $form['basic']['inline'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['basic']['inline']['query'] = array(
    '#title' => t('Search Query'),
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => t('Search this site')),
    '#default_value' => $query,
    '#size' => 20,
    '#maxlength' => 255,
  );

  $form['basic']['inline']['search_western'] = array(
    '#type' => 'checkbox',
    '#title' => t('Search Western'),
  );

  $form['basic']['inline']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Submit handler for local Thunderstone search form.
 */
function thundersearch_form_submit($form, &$form_state) {
  $profile = variable_get('thundersearch_pr', THUNDERSEARCH_DEFAULT_DRUPAL_PR);

  if ($form_state['values']['search_western'] === 1) {
    $profile = THUNDERSEARCH_DEFAULT_DRUPAL_PR;
  }

  $options = array(
    'query' => array(
      'pr' => $profile,
      'query' => $form_state['values']['query'],
    ),
  );

  // Searches from a 404 page will fail if the destination parameter is set.
  if (isset($_GET['destination'])) {
    unset($_GET['destination']);
  }

  drupal_goto(THUNDERSEARCH_RESULT_URL, $options);
}

/**
 * Thunderstone search results page callback.
 */
function thundersearch_result() {
  $query = drupal_get_query_parameters();
  $profile = variable_get('thundersearch_pr', THUNDERSEARCH_DEFAULT_DRUPAL_PR);
  $thundersearch_server_url = variable_get('thundersearch_server_url');

  $query += array(
    'pr' => $profile,
  );

  $options = array(
    'external' => TRUE,
    'absolute' => TRUE,
    'query' => $query,
  );

  $url = url($thundersearch_server_url . THUNDERSEARCH_MAIN_ENDPOINT, $options);
  $result = _thundersearch_curl_get($url);

  if ($result === FALSE) {
    drupal_set_message('An error occurred while executing the search.', 'error');
    $output = '<p>Failed to get search results.</p>';
  }
  else {
    $output = _thundersearch_parse_result($result);
  }

  return $output;
}

/**
 * Perform a get request to Thunderstone via GET.
 */
function _thundersearch_curl_get($url) {
  $handle = curl_init();

  curl_setopt($handle, CURLOPT_URL, $url);
  curl_setopt($handle, CURLOPT_HEADER, FALSE);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($handle, CURLOPT_FRESH_CONNECT, TRUE);

  $result = curl_exec($handle);

  if ($result === FALSE) {
    watchdog('Thunderstone', 'Thunderstone search error: %error.', array('%error' => curl_error($handle)), WATCHDOG_ERROR);
  }

  curl_close($handle);

  return $result;
}

/**
 * Parse query result HTML.
 *
 * Ensure that the page remains valid HTML by removing unneeded tags including
 * script, link (CSS), metatags, and title.
 */
function _thundersearch_parse_result($result) {
  global $base_url;

  $document = new DOMDocument();

  $document->loadHTML($result);

  /*****************************************************************************
   *
   * Remove the html tag.
   *
   ****************************************************************************/
  $html_tags = $document->getElementsByTagName('html');

  while ($html_tags->length > 0) {
    $html_tag = $html_tags->item(0);
    $children = $html_tag->childNodes;

    while ($children->length > 0) {
      $child = $children->item(0);
      $html_tag->parentNode->appendChild($html_tag->removeChild($child));
    }

    $html_tag->parentNode->removeChild($html_tag);
  }

  /*****************************************************************************
   *
   * Remove the body tag.
   *
   ****************************************************************************/
  $body_tags = $document->getElementsByTagName('body');

  while ($body_tags->length > 0) {
    $body_tag = $body_tags->item(0);
    $children = $body_tag->childNodes;

    while ($children->length > 0) {
      $child = $children->item(0);
      $body_tag->parentNode->appendChild($body_tag->removeChild($child));
    }

    $body_tag->parentNode->removeChild($body_tag);
  }

  /*****************************************************************************
   *
   * Set the  action of the search form to the site URL.
   *
   ****************************************************************************/
  $thunderstone_search_form = $document->getElementById('ThunderstoneSearchForm');

  if ($thunderstone_search_form) {
    $thunderstone_search_form->setAttribute('action', $base_url . THUNDERSEARCH_RESULT_URL);
  }

  /*****************************************************************************
   *
   * Replace the Thunderstone URL with the site URL in all links.
   *
   ****************************************************************************/
  $anchors_to_remove = array();

  foreach ($document->getElementsByTagName('a') as $anchor) {
    $old_href = $anchor->getAttribute('href');
    $new_href = str_replace(THUNDERSEARCH_MAIN_ENDPOINT, $base_url . THUNDERSEARCH_RESULT_URL, $old_href, $count);

    if ($count > 0) {
      $anchor->setAttribute('href', $new_href);
    }
  }

  foreach ($anchors_to_remove as $anchor) {
    $anchor->parentNode->removeChild($anchor);
  }

  /*****************************************************************************
   *
   * Move query label next to query input.
   *
   ****************************************************************************/
  if (isset($query) && $query && isset($query_label) && $query_label) {
    $query->parentNode->appendChild($query_label->parentNode->removeChild($query_label));
  }

  /*****************************************************************************
   *
   * Remove the doctype.
   *
   ****************************************************************************/
  $doctype = $document->doctype;
  $document->doctype->parentNode->removeChild($doctype);

  return $document->saveHTML();
}
