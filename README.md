# README #

1. Clone this module into /sites/all/modules
2. Enable the module
3. Set appropriate permissions for users
4. Update template.php in wwuzen lines 151-155 to the following:
function wwuzen_preprocess_page(&$variables, $hook) {
  $search_box = drupal_get_form('thundersearch_form');
  $variables['search_box'] = drupal_render($search_box);
}
5. Disable the google search appliance module and wwuzen_gsa modules
6. Go to Configuration -> Search & Metadata -> WWU TSA settings
* Put in https://search2.wwu.edu/ for the URL and the profile name obtained from Thunderstone
